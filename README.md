# Bash prompt #

Creating this to learn some shell scripting, this is my little modification of https://gist.github.com/insin/1425703 whith installation shell script.

### How it looks? ###

![prompt.png](https://bitbucket.org/repo/jGd96k/images/2461709385-prompt.png)

### How to install? ###

clone repository by 
```
#!shell

git clone git@bitbucket.org:krozycki/bash_prompt.git
```
then get inside the project and type:


```
#!shell

./install_prompt.sh
```
take it and go :)